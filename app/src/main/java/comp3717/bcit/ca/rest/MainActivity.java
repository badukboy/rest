package comp3717.bcit.ca.rest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class MainActivity
    extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void jsonRequest(final View view)
    {
        Ion.with(this).
            load("http://cartoonapi.azurewebsites.net/api/cartoon").
            asJsonArray().
            setCallback(
                new FutureCallback<JsonArray>()
            {
                @Override
                public void onCompleted(final Exception ex,
                                        final JsonArray array)
                {
                    if(ex != null)
                    {
                        Toast.makeText(MainActivity.this,
                                       "Error: " + ex.getMessage(),
                                       Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        for(final JsonElement element : array)
                        {
                            final JsonObject  json;
                            final JsonElement nameElement;
                            final JsonElement pictureUrlElement;
                            final String      name;
                            final String      pictureURL;

                            json              = element.getAsJsonObject();
                            nameElement       = json.get("name");
                            pictureUrlElement = json.get("pictureUrl");
                            name              = nameElement.getAsString();
                            pictureURL        = pictureUrlElement.getAsString();
                            Log.d("X",
                                   name + " -> " + pictureURL);
                        }
                    }
                }
            });
    }
}
